# ambient-noise-script
A script for playing ambient noises while you work

run with:
```
./ambient-noise.sh
```

you might need to make it executable using:
```
sudo chmod +x ambient-noise.sh
```
